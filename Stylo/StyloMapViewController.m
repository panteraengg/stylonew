//
//  StyloMapViewController.m
//  Stylo
//
//  Created by Pantera Engineering on 23/04/2014.
//  Copyright (c) 2014 Pantera Engineering. All rights reserved.
//

#import "StyloMapViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface storeLocation ()
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) CLLocationCoordinate2D theCoordinate;
@end

@implementation storeLocation

- (id)initWithCity:(NSString*)city name:(NSString*)name coordinate:(CLLocationCoordinate2D)coordinate {
    if ((self = [super init])) {
            self.city = city;
        self.name = name;
        self.theCoordinate = coordinate;
    }
    return self;
}

- (NSString *)title {
    return _city;
}

- (NSString *)subtitle {
    return _name;
}

- (CLLocationCoordinate2D)coordinate {
    return _theCoordinate;
}

- (MKMapItem*)mapItem {
    
    MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:self.coordinate
                              addressDictionary:nil];

    MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
    mapItem.name = self.title;
    
    return mapItem;
}

@end

@interface StyloMapViewController ()

@end

@implementation StyloMapViewController{
    BOOL viewIsOpen;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _mapView.delegate = self;
    
    _city.text = [_selectedStore objectForKey:@"City"];
    _name.text = [_selectedStore objectForKey:@"Name"];
    _address.text = [_selectedStore objectForKey:@"Address"];
    _distance.text = [NSString stringWithFormat:@"%.2f KM",[[_selectedStore objectForKey:@"distance"] floatValue]];
    if ([[_selectedStore objectForKey:@"Telephone"] length]>0) {
        [_phone setTitle:[_selectedStore objectForKey:@"Telephone"] forState:UIControlStateNormal];
    }else{
        [_phone removeFromSuperview];
    }
    
    if([[_selectedStore objectForKey:@"Cell"] length]>0){
        [_cell setTitle:[_selectedStore objectForKey:@"Cell"] forState:UIControlStateNormal];
    }else{
        [_phone removeFromSuperview];
    }
    viewIsOpen = YES;
    
    UINavigationItem *title = [_navigationBar.items objectAtIndex:0];
    
    title.title = [_selectedStore objectForKey:@"Name"];
    //[self.navigationBar setTitle:[_selectedStore objectForKey:@"Name"]];
    [self addAnnotationsToMap];
}
-(void)addAnnotationsToMap{
    
    for (NSDictionary *row in _stores) {
        float latitude = [[row objectForKey:@"Latitude"] floatValue];
        float longitude = [[row objectForKey:@"Longitude"] floatValue];
        NSString *storeCity = [row objectForKey:@"City"];
        NSString *storeName = [row objectForKey:@"Name"];
        
        CLLocationCoordinate2D coordinate;
        coordinate.latitude = latitude;
        coordinate.longitude = longitude;
        storeLocation *annotation = [[storeLocation alloc] initWithCity:storeCity name:storeName coordinate:coordinate];
        [_mapView addAnnotation:annotation];
	}
    
}
- (void)viewWillAppear:(BOOL)animated {
    
    CLLocationCoordinate2D storeLocation;
    storeLocation.latitude = [[_selectedStore objectForKey:@"Latitude"] floatValue];
    storeLocation.longitude= [[_selectedStore objectForKey:@"Longitude"] floatValue];
    
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(storeLocation, 500, 500);
    
    [_mapView setRegion:viewRegion animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - MAP View Delegate
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    
    static NSString *identifier = @"storeLocation";
    
    MKAnnotationView *annotationView = (MKAnnotationView *) [_mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
    if (annotationView == nil) {
        annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
        annotationView.enabled = YES;
        annotationView.canShowCallout = YES;
        annotationView.image = [UIImage imageNamed:@"locator_icon.png"];
    } else {
        annotationView.annotation = annotation;
    }
        
    return annotationView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - IB Actions
- (IBAction)cancel:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)toggleView:(id)sender {
    CABasicAnimation *popupAnimation;
    
    popupAnimation = [CABasicAnimation animationWithKeyPath:@"position"];
    popupAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    
    CGPoint endPoint;
    
    if(viewIsOpen){
        
        endPoint = CGPointMake(160, _descriptionView.layer.position.y-106);
        viewIsOpen = NO;
        [_toggleButton setImage:[UIImage imageNamed:@"up.png"] forState:UIControlStateNormal];
        
    }else{
        
        endPoint = CGPointMake(160, _descriptionView.layer.position.y+106);
        viewIsOpen = YES;
        [_toggleButton setImage:[UIImage imageNamed:@"down.png"] forState:UIControlStateNormal];
        
    }
    
    
    popupAnimation.fromValue = [NSValue valueWithCGPoint:_descriptionView.layer.position];
    popupAnimation.toValue = [NSValue valueWithCGPoint:endPoint];
    popupAnimation.repeatCount = 1;
    popupAnimation.duration = 0.5;
    [_descriptionView.layer addAnimation:popupAnimation forKey:@"position"];
    [_descriptionView.layer setPosition:endPoint];
}
- (IBAction)callLandline:(id)sender {
    UIDevice *device = [UIDevice currentDevice];
    if ([[device model] isEqualToString:@"iPhone"] ) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",[_selectedStore objectForKey:@"Telephone"]]]];
    } else {
        UIAlertView *notPermitted=[[UIAlertView alloc] initWithTitle:@"Sorry!" message:[NSString stringWithFormat:@"Your device does not support call feature."] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [notPermitted show];
    }
    
}
- (IBAction)callCellPhone:(id)sender {
    UIDevice *device = [UIDevice currentDevice];
    if ([[device model] isEqualToString:@"iPhone"] ) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",[_selectedStore objectForKey:@"Cell"]]]];
    } else {
        UIAlertView *notPermitted=[[UIAlertView alloc] initWithTitle:@"Sorry!" message:@"Your device does not support call feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [notPermitted show];
    }
}
@end
