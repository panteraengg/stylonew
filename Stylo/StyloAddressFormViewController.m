//
//  StyloBillingInformationViewController.m
//  Stylo
//
//  Created by Pantera Engineering on 30/04/2014.
//  Copyright (c) 2014 Pantera Engineering. All rights reserved.
//

#import "StyloAddressFormViewController.h"
#import "StyloAppDelegate.h"
#import "StyloAddressListViewController.h"
#import "StyloNavigationViewController.h"
#import "StyloSaveInfoViewController.h"
#import "StyloConfirmOrderViewController.h"


@interface StyloAddressFormViewController ()

@end

@implementation StyloAddressFormViewController{
    UIColor *styloPrimaryColor;
    UIColor *styloSecondaryColor;
    StyloAppDelegate *appDelegate;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    appDelegate = [[UIApplication sharedApplication] delegate];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 20)];
    styloPrimaryColor = [UIColor colorWithRed:92/255.0 green:45/255.0 blue:145/255.0 alpha:1];
    styloSecondaryColor = [UIColor colorWithRed:243/255.0 green:145/255.0 blue:188/255.0 alpha:1];
    

    CGRect frameRect1 = _fname.frame;
    CGRect frameRect2 = _lname.frame;
    CGRect frameRect3 = _email.frame;
    CGRect frameRect4 = _phoneNumber.frame;
    CGRect frameRect5 = _address.frame;
    CGRect frameRect6 = _city.frame;
    frameRect1.size.height = 40;
    frameRect2.size.height = 40;
    frameRect3.size.height = 40;
    frameRect4.size.height = 40;
    frameRect5.size.height = 40;
    frameRect6.size.height = 40;
    _fname.frame = frameRect1;
    _lname.frame=frameRect2;
    _email.frame=frameRect3;
    _phoneNumber.frame=frameRect4;
    _address.frame=frameRect5;
    _city.frame=frameRect6;
    
   /* for(id x in [self.view subviews]){
        if([x isKindOfClass:[UITextField class]])
        {
            UITextField *textField = x;
            textField.layer.borderColor = [styloSecondaryColor CGColor];
            textField.layer.borderWidth = 1.f;
            textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: styloSecondaryColor}];
            textField.leftView = paddingView;
            textField.layer.sublayerTransform = CATransform3DMakeTranslation(5.5, 0, 0);
        }
    }
    */
    
    if (_isShipping) {
        [self.navigationItem setTitle:@"Shipping Address"];
        [_email removeFromSuperview];
        [_phoneNumber setFrame:CGRectMake(_phoneNumber.frame.origin.x, _phoneNumber.frame.origin.y-44, _phoneNumber.frame.size.width, _phoneNumber.frame.size.height)];
        [_address setFrame:CGRectMake(_address.frame.origin.x, _address.frame.origin.y-44, _address.frame.size.width, _address.frame.size.height)];
        [_city setFrame:CGRectMake(_city.frame.origin.x, _city.frame.origin.y-44, _city.frame.size.width, _city.frame.size.height)];
    }else{
        [self.navigationItem setTitle:@"Billing Address"];
    }
    
    UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:nil action:nil];
    [self.view addGestureRecognizer:panRecognizer];
    [self.navigationController.navigationBar addGestureRecognizer:panRecognizer];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)shakeItBaby:(UIView *)viewToShake{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
    [animation setDuration:0.05];
    [animation setRepeatCount:4];
    [animation setAutoreverses:YES];
    [animation setFromValue:[NSValue valueWithCGPoint:
                             CGPointMake([viewToShake center].x - 5.0f, [viewToShake center].y)]];
    [animation setToValue:[NSValue valueWithCGPoint:
                           CGPointMake([viewToShake center].x + 5.0f, [viewToShake center].y)]];
    
    [viewToShake.layer addAnimation:animation forKey:@"key"];
}
-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}


#pragma mark - Text field delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField.tag >= 100 && textField.tag<105){
        [[self.view viewWithTag:textField.tag+1] becomeFirstResponder];
    }else if (textField.tag == 105){
        [self.view endEditing:YES];
    }
    return YES;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    if(textField.text.length >0){
        textField.layer.borderColor = [styloSecondaryColor CGColor];
    }else{
        textField.layer.borderColor = [styloSecondaryColor CGColor];
    }
    textField.layer.borderWidth = 1.0f;
    if (textField.tag == 105) {
        
        [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+44, self.view.frame.size.width, self.view.frame.size.height)];
    }
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {

    textField.clipsToBounds = NO;

    textField.layer.borderColor = [styloPrimaryColor CGColor];
    textField.layer.borderWidth = 2.0f;
    if (textField.tag == 105) {
        
        [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-44, self.view.frame.size.width, self.view.frame.size.height)];
    }
    
    return YES;
}
#pragma mark - Interface Builder Actions
- (IBAction)releaseFocus:(id)sender {
    [self.view endEditing:YES];
}

- (IBAction)continueToShipping:(id)sender {
    if(_fname.text.length==0){
        [self shakeItBaby:_fname];
    }else if(_lname.text.length==0){
        [self shakeItBaby:_lname];
    }else if (!_isShipping && ![self NSStringIsValidEmail:_email.text]){
        [self shakeItBaby:_email];
    }else if (_phoneNumber.text.length<7){
        [self shakeItBaby:_phoneNumber];
    }else if (_address.text.length==0){
        [self shakeItBaby:_address];
    }else if (_city.text.length==0){
        [self shakeItBaby:_city];
    }else{
        if (_isShipping) {
            [_checkoutData setObject:_fname.text forKey:@"Sfname"];
            [_checkoutData setObject:_lname.text forKey:@"Slname"];
            [_checkoutData setObject:_phoneNumber.text forKey:@"Sphone"];
            [_checkoutData setObject:_address.text forKey:@"Saddress"];
            [_checkoutData setObject:_city.text forKey:@"Scity"];
            [_checkoutData setObject:[NSNumber numberWithBool:YES] forKey:@"saveAddressShip"];
            
            StyloConfirmOrderViewController *confirmScreen  = [self.storyboard instantiateViewControllerWithIdentifier:@"confirmPage"];
            confirmScreen.checkoutData = _checkoutData;
            [self.navigationController pushViewController:confirmScreen animated:YES];
        }else{
            //Adding Billing Info
            [_checkoutData setObject:_fname.text forKey:@"fname"];
            [_checkoutData setObject:_lname.text forKey:@"lname"];
            [_checkoutData setObject:_email.text forKey:@"email"];
            [_checkoutData setObject:_phoneNumber.text forKey:@"phone"];
            [_checkoutData setObject:_address.text forKey:@"address"];
            [_checkoutData setObject:_city.text forKey:@"city"];
            [_checkoutData setObject:[NSNumber numberWithBool:YES] forKey:@"saveAddressBill"];
            
            if (appDelegate.isLoggedIn) {
                
                StyloAddressListViewController *shippingAddressPage = [self.storyboard instantiateViewControllerWithIdentifier:@"shippingView"];
                
                shippingAddressPage.isShipping = YES;
                shippingAddressPage.checkoutData = _checkoutData;
                [self.navigationController pushViewController:shippingAddressPage animated:YES];
            }else{
                StyloSaveInfoViewController *saveInfoPage = [self.storyboard instantiateViewControllerWithIdentifier:@"saveAsAccountPage"];
                saveInfoPage.checkoutData = _checkoutData;
                [self.navigationController pushViewController:saveInfoPage animated:YES];
            }
        }
    }
    
}
@end
