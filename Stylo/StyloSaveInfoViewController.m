//
//  StyloSaveInfoViewController.m
//  Stylo
//
//  Created by Pantera Engineering on 10/05/2014.
//  Copyright (c) 2014 Pantera Engineering. All rights reserved.
//

#import "StyloSaveInfoViewController.h"
#import "StyloAddressListViewController.h"
#import "StyloNavigationViewController.h"

@interface StyloSaveInfoViewController ()
@property (strong, nonatomic) IBOutlet UITextField *password1;
@property (strong, nonatomic) IBOutlet UITextField *password2;

@end

@implementation StyloSaveInfoViewController{
    UIColor *styloPrimaryColor;
    UIColor *styloSecondaryColor;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:nil action:nil];
    [self.view addGestureRecognizer:panRecognizer];
    [self.navigationController.navigationBar addGestureRecognizer:panRecognizer];
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 20)];
    styloPrimaryColor = [UIColor colorWithRed:92/255.0 green:45/255.0 blue:145/255.0 alpha:1];
    styloSecondaryColor = [UIColor colorWithRed:243/255.0 green:145/255.0 blue:188/255.0 alpha:1];
    for(id x in [self.view subviews]){
        if([x isKindOfClass:[UITextField class]])
        {
            UITextField *textField = x;
            textField.layer.borderColor = [styloSecondaryColor CGColor];
            textField.layer.borderWidth = 1.f;
            textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: styloSecondaryColor}];
            textField.delegate = self;
            textField.leftView = paddingView;
            textField.layer.sublayerTransform = CATransform3DMakeTranslation(5.5, 0, 0);
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)shakeItBaby:(UIView *)viewToShake{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
    [animation setDuration:0.07];
    [animation setRepeatCount:3];
    [animation setAutoreverses:YES];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [animation setFromValue:[NSValue valueWithCGPoint:
                             CGPointMake([viewToShake center].x - 5.0f, [viewToShake center].y)]];
    [animation setToValue:[NSValue valueWithCGPoint:
                           CGPointMake([viewToShake center].x + 5.0f, [viewToShake center].y)]];
    
    [viewToShake.layer addAnimation:animation forKey:@"key"];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Interface Builder Actions
- (IBAction)releaseFocus:(id)sender {
    [self.view endEditing:YES];
}
- (IBAction)saveAsAccount:(id)sender {
    
    if (_password1.text.length==0) {
        [self shakeItBaby:_password1];
    }else if(![_password1.text isEqualToString:_password2.text]){
        [self shakeItBaby:_password2];
    }else{
        [_checkoutData setObject:[_password1 text] forKey:@"password1"];
        [_checkoutData setObject:[_password2 text] forKey:@"password2"];
        [_checkoutData setObject:[NSNumber numberWithBool:YES] forKey:@"signup"];
        StyloAddressListViewController *shippingAddress = [self.storyboard instantiateViewControllerWithIdentifier:@"shippingView"];
        shippingAddress.isShipping=YES;
        shippingAddress.checkoutData = _checkoutData;
        [self.navigationController performSelector:@selector(replaceLastWith:) withObject:shippingAddress];
    }
}
- (IBAction)continueWithoutSaving:(id)sender {
    [_checkoutData setObject:[NSNumber numberWithBool:NO] forKey:@"signup"];
    StyloAddressListViewController *shippingAddress = [self.storyboard instantiateViewControllerWithIdentifier:@"shippingView"];
    shippingAddress.isShipping=YES;
    shippingAddress.checkoutData = _checkoutData;
    [self.navigationController performSelector:@selector(replaceLastWith:) withObject:shippingAddress];
}
#pragma mark - Text field delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField.tag >= 100 && textField.tag<104){
        [[self.view viewWithTag:textField.tag+1] becomeFirstResponder];
    }else if (textField.tag == 104){
        [self.view endEditing:YES];
    }
    return YES;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    if(textField.text.length >0){
        textField.layer.borderColor = [styloSecondaryColor CGColor];
    }else{
        textField.layer.borderColor = [styloSecondaryColor CGColor];
    }
    textField.layer.borderWidth = 1.0f;
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    textField.clipsToBounds = NO;
    
    textField.layer.borderColor = [styloPrimaryColor CGColor];
    textField.layer.borderWidth = 2.0f;
    
    return YES;
}
@end
