//
//  StyloNavigationViewController.m
//  Stylo
//
//  Created by Pantera Engineering on 19/04/2014.
//  Copyright (c) 2014 Pantera Engineering. All rights reserved.
//

#import "StyloNavigationViewController.h"
#import "StyloSlidingMenuViewController.h"

@interface StyloNavigationViewController ()

@end

@implementation StyloNavigationViewController{
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.view.layer.shadowOpacity = 0.75f;
    self.view.layer.shadowRadius = 10.0f;
    self.view.layer.shadowColor = [UIColor blackColor].CGColor;
    if (![self.slidingViewController.underLeftViewController/* underRightViewController*/ isKindOfClass:[StyloSlidingMenuViewController class]]) {
        StyloSlidingMenuViewController *sidePanel = [self.storyboard instantiateViewControllerWithIdentifier:@"LeftMenu"];
        sidePanel.navController = self;
        self.slidingViewController.underLeftViewController /*underRightViewController*/ = sidePanel;
        /* self.slidingViewController.underRightViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"LeftMenu"];
        */
        
    }
    self.slidingViewController.shouldAddPanGestureRecognizerToTopViewSnapshot = YES;
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
}
-(void)revealMenu
{
    [self.slidingViewController anchorTopViewTo:ECRight];
}
-(void)backButtonTapped{
    
    NSLog(@"back BUTTON PRESSED");
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(void)reloadMenu{
    StyloSlidingMenuViewController *menu = (StyloSlidingMenuViewController *)self.slidingViewController.underLeftViewController/*underRightViewController*/;
    [menu.menuTableView reloadData];
}
- (void) replaceLastWith:(UIViewController *) controller {
    NSMutableArray *stackViewControllers = [NSMutableArray arrayWithArray:self.viewControllers];
    [stackViewControllers removeLastObject];
    [stackViewControllers addObject:controller];
    [self setViewControllers:stackViewControllers animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
