//
//  StyloshippingInformationViewController.h
//  Stylo
//
//  Created by Pantera Engineering on 09/05/2014.
//  Copyright (c) 2014 Pantera Engineering. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StyloAddressListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *addressesTableView;
@property (strong, nonatomic) NSMutableDictionary *checkoutData;
@property (assign, nonatomic) BOOL isShipping;

@end
