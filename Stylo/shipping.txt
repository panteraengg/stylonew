1.	DELIVERY TIME:

	We provide shipping all over Pakistan through TCS. Deliveries take 3-5 business days.

2.	LISTING AND DESCRIPTIONS:

	Stylo Shoes makes every effort to ensure that the product descriptions and photographs provided on this web site are accurate and life-like. Whilst the reproduction of colors and styles contained on the photographs shown on this web site are as accurate as image and photographic processing will allow, Stylo Shoes cannot accept responsibility for slight variances in color or style.

3.	AMEND YOUR ORDERS:

	Any amendments can be suggested within 24 hours from the receipt of the order. Any amendment to the original order after 24 hours may be subject to additional charges which will be determined after reviewing the requested changes.

4.	Returns and Exchange:

	If you wish to return the merchandize purchased online, you may do so in a period no later than 14 days from the day of purchase. You must deliver the merchandise in the same packaging in which you received it. To schedule a return, please contact us at support@styloshoes.com.pk in order for us to arrange the collection at your home address.
 We will fully examine the returned product and will notify you of your right to a refund of the amounts paid. The refund will be made as soon as possible, and within 30 calendar days from the date on which you informed us of your intention to withdraw. The refund will be made using the same payment method which was used to make the purchase.
 No exchange or refund will be made for any products that are not in the same condition in which you received them, or that have been used beyond mere opening of the product.

5.	Exchange:

	You can only make exchanges for the same article in a different size or color.
