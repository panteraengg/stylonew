//
//  StyloSaveInfoViewController.h
//  Stylo
//
//  Created by Pantera Engineering on 10/05/2014.
//  Copyright (c) 2014 Pantera Engineering. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StyloSaveInfoViewController : UIViewController <UITextFieldDelegate>
@property (nonatomic, strong) NSMutableDictionary *checkoutData;

@end
