//
//  StyloStoreLocatorViewController.h
//  Stylo
//
//  Created by Pantera Engineering on 22/04/2014.
//  Copyright (c) 2014 Pantera Engineering. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface StyloStoreLocatorViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate,CLLocationManagerDelegate>
@property (strong, nonatomic) IBOutlet UILabel *statusLabel;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UITableView *storeTable;

@end
