//
//  StyloItemsDetailViewController.h
//  Stylo
//
//  Created by Pantera Engineering on 21/04/2014.
//  Copyright (c) 2014 Pantera Engineering. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>

@interface StyloItemsDetailViewController : UIViewController <UIScrollViewDelegate>
@property NSUInteger pageIndex;
@property NSDictionary *item;
@property (strong, nonatomic) IBOutlet UILabel *itemName;
@property (strong, nonatomic) IBOutlet UILabel *itemDescription;
@property (strong, nonatomic) IBOutlet UILabel *itemPrice;
@property (strong, nonatomic) IBOutlet UIScrollView *itemScrollView;
@property (strong, nonatomic) IBOutlet UIView *viewForSegments;
@property (strong, nonatomic) IBOutlet UIView *popupView;
@property (strong, nonatomic) IBOutlet UIButton *popupButton;
@property (strong, nonatomic) IBOutlet UISegmentedControl *sizesSegmentedControl;
@property (strong, nonatomic) IBOutlet UISegmentedControl *quantitySegmentedControl;

- (IBAction)togglePopup:(id)sender;
- (IBAction)newSizeSelected:(id)sender;
- (IBAction)addToCart:(id)sender;
- (IBAction)shareIt:(id)sender;

@end
