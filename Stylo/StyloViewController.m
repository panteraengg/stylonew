//
//  StyloViewController.m
//  Stylo
//
//  Created by Pantera Engineering on 19/04/2014.
//  Copyright (c) 2014 Pantera Engineering. All rights reserved.
//

#import "StyloViewController.h"
#import "ECSlidingViewController.h"
#import "StyloNavigationViewController.h"
#import "StyloItemsViewController.h"

@interface StyloViewController ()

@end

@implementation StyloViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIImageView *myImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stylo"]];
    self.navigationItem.titleView = myImageView;
    
    self.scrollView.contentSize = CGSizeMake(320, 950/*522*/);

	// Do any additional setup after loading the view, typically from a nib.
}
- (IBAction)revealMyMenu:(id)sender {
    StyloNavigationViewController *navController = (StyloNavigationViewController *)self.navigationController;
    [navController revealMenu];
    //[self.navigationController.slidingViewController anchorTopViewTo:ECLeft];
}
- (IBAction)showMyCart:(id)sender {
    [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"cartView"] animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqual:@"shoeSegue"]){
        
        StyloItemsViewController *destination = [segue destinationViewController];
        destination.targetURL = @"http://54.179.143.160/shoes.json";
        
    } else if ([segue.identifier isEqualToString:@"kidzSegue"]){
        
        StyloItemsViewController *destination = [segue destinationViewController];
        destination.targetURL = @"http://54.179.143.160/kids.json";
        
    } else if ([segue.identifier isEqualToString:@"fabrizioSegue"]){
        
        StyloItemsViewController *destination = [segue destinationViewController];
        destination.targetURL = @"http://54.179.143.160/fabrizio.json";
        
    }
}
#pragma  mark - IB Actions
- (IBAction)showFacebook:(id)sender {
    
    NSURL *facebookURL = [NSURL URLWithString:@"fb://profile/306362919556"];
    if ([[UIApplication sharedApplication] canOpenURL:facebookURL]) {
        [[UIApplication sharedApplication] openURL:facebookURL];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://facebook.com/StyloShoes"]];
    }
    
}
- (IBAction)showTwitter:(id)sender {
    
    NSURL *twitterURL = [NSURL URLWithString:@"twitter:///user?screen_name=StyloShoesPK"];
    if ([[UIApplication sharedApplication] canOpenURL:twitterURL]) {
        [[UIApplication sharedApplication] openURL:twitterURL];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://twitter.com/StyloShoesPK"]];
    }
    
}
- (IBAction)callStylo:(id)sender {
    
    UIDevice *device = [UIDevice currentDevice];
    if ([[device model] isEqualToString:@"iPhone"] ) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:0800-78956"]]];
    } else {
        UIAlertView *notPermitted=[[UIAlertView alloc] initWithTitle:@"Cannot Call" message:@"Your device does not support call feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [notPermitted show];
    }
    
}

@end
