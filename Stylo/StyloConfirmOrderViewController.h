//
//  StyloConfirmOrderViewController.h
//  Stylo
//
//  Created by Pantera Engineering on 12/05/2014.
//  Copyright (c) 2014 Pantera Engineering. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StyloConfirmOrderViewController : UIViewController <NSURLConnectionDelegate, NSURLConnectionDataDelegate>

@property (nonatomic, strong) NSMutableDictionary *checkoutData;

@end
