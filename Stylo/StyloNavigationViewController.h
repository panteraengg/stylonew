//
//  StyloNavigationViewController.h
//  Stylo
//
//  Created by Pantera Engineering on 19/04/2014.
//  Copyright (c) 2014 Pantera Engineering. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <QuartzCore/QuartzCore.h>
#import "ECSlidingViewController.h"
#import "StyloSlidingMenuViewController.h"

@interface StyloNavigationViewController : UINavigationController

- (void) replaceLastWith:(UIViewController *) controller;
-(void)revealMenu;
-(void)reloadMenu;
@end
