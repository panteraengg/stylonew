//
//  StyloItemsTableTableViewController.m
//  Stylo
//
//  Created by Pantera Engineering on 19/04/2014.
//  Copyright (c) 2014 Pantera Engineering. All rights reserved.
//

#import "StyloItemsTableViewController.h"
#import "UIImageView+WebCache.h"
#import "StyloItemsDetailViewController.h"


@interface StyloItemsTableViewController ()
@property (strong, nonatomic) UIPageViewController *pageViewController;
@end

@implementation StyloItemsTableViewController{

   
    IBOutlet UICollectionView *itemsCollectionView;
  
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.hidesBackButton = YES;
  /*  UIBarButtonItem *newBackButton = [[UIBarButtonItem alloc] initWithTitle: @"HAHA" style: UIBarButtonItemStyleBordered target: nil action: @selector(backButtonTapped)];
    
    // [[self navigationItem] setBackBarButtonItem: newBackButton];
    self.navigationItem.leftBarButtonItem = newBackButton;
   */
 

    _pageAnimationFinished = YES;
    self.pageViewController.delegate = self;
    
    //self.tableView.delegate = self;
    
}
-(void)backButtonTapped{
    NSLog(@"BACK FOR 2");

    [self.navigationController dismissViewControllerAnimated:NO completion:nil];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [_parent moveSegmentedControlToindex:_pageIndex];
    //[self.tableView reloadData];
    [itemsCollectionView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Collection View Delegate Methods


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [[_styloType objectForKey:@"products"] count];
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"itemCell" forIndexPath:indexPath];
  
    UIImageView *thumb = (UIImageView *)[cell viewWithTag:1];
    UILabel *name = (UILabel *)[cell viewWithTag:2];
  //  UILabel *description = (UILabel *) [cell viewWithTag:3];
    UILabel *price = (UILabel *) [cell viewWithTag:4];
    
    float priceValue = (float)[[[[_styloType objectForKey:@"products"] objectAtIndex:indexPath.row] objectForKey:@"price"] floatValue];
    NSString *thumbURL = [[[_styloType objectForKey:@"products"] objectAtIndex:indexPath.row] objectForKey:@"thumb"];
    
    NSString *nameText = [[[_styloType objectForKey:@"products"] objectAtIndex:indexPath.row] objectForKey:@"name"];
    NSRange range = [nameText rangeOfString:@"("];
    
    [thumb setImageWithURL:[NSURL URLWithString:thumbURL] placeholderImage:[UIImage imageNamed:@"loading.gif"]];
    
    if (range.location != NSNotFound) {
        name.text = [nameText substringToIndex:range.location];
    }else {
        name.text = nameText;
    }
  //  description.text = [[[_styloType objectForKey:@"products"] objectAtIndex:indexPath.row] objectForKey:@"short_description"];
    
  //  description.text = [[[_styloType objectForKey:@"products"] objectAtIndex:indexPath.row] objectForKey:@"short_description"];
    price.text = [NSString stringWithFormat:@"PKR %.2f",priceValue];
    
    return cell;
    
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
   /*StyloItemsDetailViewController *detailView = (StyloItemsDetailViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"itemDetails"];
    detailView.item = [[_styloType objectForKey:@"products"] objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:detailView animated:YES];
    [detailView.navigationItem setTitle:[_styloType objectForKey:@"name"]];
    */
    
    UIViewController *viewController = [[UIViewController alloc] init];
    // Create page view controller
    
    [self.navigationController pushViewController:viewController animated:YES];
    [viewController.view setBackgroundColor:[UIColor whiteColor]];
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"detailsPageViewController"];
    self.pageViewController.dataSource = self;
    NSLog(@"item tapped");
    StyloItemsDetailViewController *startingViewController = [self viewControllerAtIndex:indexPath.row];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    // Change the size of page view controller
    self.pageViewController.view.frame = CGRectMake(0,0 /*-94*/, self.view.frame.size.width, self.view.frame.size.height/*+94*/);
    [viewController addChildViewController:_pageViewController];
   [viewController.view addSubview:_pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];

    
    
    
}
/*
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [[_styloType objectForKey:@"products"] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"itemCell" forIndexPath:indexPath];
    
    UIImageView *thumb = (UIImageView *)[cell viewWithTag:1];
    UILabel *name = (UILabel *)[cell viewWithTag:2];
    UILabel *description = (UILabel *) [cell viewWithTag:3];
    UILabel *price = (UILabel *) [cell viewWithTag:4];
    
    float priceValue = (float)[[[[_styloType objectForKey:@"products"] objectAtIndex:indexPath.row] objectForKey:@"price"] floatValue];
    NSString *thumbURL = [[[_styloType objectForKey:@"products"] objectAtIndex:indexPath.row] objectForKey:@"thumb"];
    
    NSString *nameText = [[[_styloType objectForKey:@"products"] objectAtIndex:indexPath.row] objectForKey:@"name"];
    NSRange range = [nameText rangeOfString:@"("];
    
    [thumb setImageWithURL:[NSURL URLWithString:thumbURL] placeholderImage:[UIImage imageNamed:@"loading.gif"]];
    
    if (range.location != NSNotFound) {
        name.text = [nameText substringToIndex:range.location];
    }else {
        name.text = nameText;
    }
    description.text = [[[_styloType objectForKey:@"products"] objectAtIndex:indexPath.row] objectForKey:@"short_description"];
    
    description.text = [[[_styloType objectForKey:@"products"] objectAtIndex:indexPath.row] objectForKey:@"short_description"];
    price.text = [NSString stringWithFormat:@"PKR %.2f",priceValue];
    // Configure the cell...
    
    return cell;
}


#pragma mark - UITableView Delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    StyloItemsDetailViewController *detailView = (StyloItemsDetailViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"itemDetails"];
    detailView.item = [[_styloType objectForKey:@"products"] objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:detailView animated:YES];
    [detailView.navigationItem setTitle:[_styloType objectForKey:@"name"]];

}

*/


- (StyloItemsDetailViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([[_styloType objectForKey:@"products"] count] == 0) || (index >= [[_styloType objectForKey:@"products"] count])) {
        return nil;
    }
    
    // Create a new view controller and pass suitable data.

    StyloItemsDetailViewController *detailView = (StyloItemsDetailViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"itemDetails"];
    detailView.item = [[_styloType objectForKey:@"products"] objectAtIndex:index];
    //[self.navigationController pushViewController:detailView animated:YES];
    [detailView.navigationItem setTitle:[_styloType objectForKey:@"name"]];

    detailView.pageIndex = index;
    
    return detailView;
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((StyloItemsDetailViewController*) viewController).pageIndex;
    
    if (_pageAnimationFinished==NO) {
        return  nil;
    }
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
   // pageViewController.view.userInteractionEnabled = NO;
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((StyloItemsDetailViewController*) viewController).pageIndex;
    if (_pageAnimationFinished==NO) {
        return  nil;
    }
    
    if (index == NSNotFound) {
        return nil;
    }
   //// if (_pageAnimationFinished){
   ////     pageViewController.view.userInteractionEnabled = YES;
   // }
    index++;
    if (index == [[_styloType objectForKey:@"products"] count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [[_styloType objectForKey:@"products"] count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}
- (void)pageViewController:(UIPageViewController *)pageViewController willTransitionToViewControllers:(NSArray *)pendingViewControllers {
    _pageAnimationFinished = NO;
}

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed {
    _pageAnimationFinished = YES;
}

@end
