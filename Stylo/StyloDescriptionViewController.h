//
//  StyloDescriptionViewController.h
//  Stylo
//
//  Created by Pantera Engineering on 07/05/2014.
//  Copyright (c) 2014 Pantera Engineering. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StyloDescriptionViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextView *descriptionText;
@property (assign, nonatomic) int descriptionType;

@end
