//
//  StyloShoppingCartViewController.m
//  Stylo
//
//  Created by Pantera Engineering on 30/04/2014.
//  Copyright (c) 2014 Pantera Engineering. All rights reserved.
//

#import "StyloShoppingCartViewController.h"
#import "StyloNavigationViewController.h"
#import "StyloLoginPageViewController.h"
#import "MBProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "StyloAppDelegate.h"
#import "StyloAddressListViewController.h"

@interface StyloShoppingCartViewController ()

@end

@implementation StyloShoppingCartViewController{
    MBProgressHUD *hud;
    NSArray *cartItems;
    NSString *grandTotal;
    NSURLConnection *fetchCartConnection;
    StyloAppDelegate *appDelegate;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = @"Shopping Cart";
    
    appDelegate = [[UIApplication sharedApplication] delegate];
    
   /* UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"list"] style:UIBarButtonItemStyleBordered target:self.navigationController action:@selector(revealMenu)];
    [self.navigationItem setRightBarButtonItem:barButton];
    */
    cartItems = [[NSArray alloc] init];
    
    NSURL *cartURL = [NSURL URLWithString:@"http://54.179.143.160/cart.php"];
    NSURLRequest *cartRequest = [NSURLRequest requestWithURL:cartURL];
    fetchCartConnection = [NSURLConnection connectionWithRequest:cartRequest delegate:self];
    [fetchCartConnection start];
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Fetching Cart";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Table View Data Source
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //return [cartItems count];
    return [cartItems count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cartCell"];
    
    
    UIImageView *itemThumb = (UIImageView *)[cell viewWithTag:1];
    UILabel *itemTitle = (UILabel *)[cell viewWithTag:2];
    UILabel *itemQuantity = (UILabel *)[cell viewWithTag:3];
    UILabel *itemPrice = (UILabel *)[cell viewWithTag:4];
    
    NSString *thumbURL = [[cartItems objectAtIndex:indexPath.row] objectForKey:@"thumb"];
    [itemThumb setImageWithURL:[NSURL URLWithString:thumbURL] placeholderImage:[UIImage imageNamed:@"loading.gif"]];
    
    NSString *name = [[cartItems objectAtIndex:indexPath.row] objectForKey:@"name"];
    NSRange range = [name rangeOfString:@"("];
    
    if (range.location != NSNotFound) {
        itemTitle.text = [name substringToIndex:range.location];
    }else {
        itemTitle.text = name;
    }
    itemQuantity.text = [NSString stringWithFormat:@"%li",(long)[[[cartItems objectAtIndex:indexPath.row] objectForKey:@"qty"] integerValue]];
    itemPrice.text = [NSString stringWithFormat:@"%li",(long)[[[cartItems objectAtIndex:indexPath.row] objectForKey:@"totalPrice"] integerValue]];
    
    return cell;
}
#pragma mark - Connection Method
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {

}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)d {

    NSDictionary *response = [NSJSONSerialization JSONObjectWithData:d options:NSJSONReadingMutableContainers error:nil];
    
    [hud hide:YES];
    if ([[response objectForKey:@"code"]  isEqual:@"101"]) {
        [hud hide:YES];
        [[[UIAlertView alloc] initWithTitle:@"Empty Cart" message:[response objectForKey:@"message"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"") otherButtonTitles:nil] show];
    }else if ([[response objectForKey:@"code"] isEqual:@"104"]){
        [hud hide:YES];
        [[[UIAlertView alloc] initWithTitle:@"Remove Product" message:[response objectForKey:@"message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"") otherButtonTitles:nil] show];
        [fetchCartConnection start];
    }else{
        
        hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
        
        hud.mode = MBProgressHUDModeCustomView;
        
        hud.labelText = @"Success";
        [hud hide:YES afterDelay:1];
        cartItems =[response objectForKey:@"products"];
        grandTotal = [response objectForKey:@"grand_total"];
        _totalLabel.text = [NSString stringWithFormat:@"Rs. %.2f",[grandTotal floatValue]];
        
        [_shoppingCartTableView reloadData];
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"")
                                message:[error localizedDescription]
                               delegate:nil
                      cancelButtonTitle:NSLocalizedString(@"OK", @"")
                      otherButtonTitles:nil] show];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    // Do anything you want with it
    
}

#pragma mark - Alert View Delegate

-(void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

#pragma mark - IB Actions

- (IBAction)removeProduct:(id)sender {
    UITableViewCell *owningCell = (UITableViewCell*)[[[sender superview] superview] superview];
    NSIndexPath *pathToCell = [_shoppingCartTableView indexPathForCell:owningCell];
    NSInteger row = pathToCell.row;
    
    NSString *removeString = [NSString stringWithFormat:@"id=%@",[[cartItems objectAtIndex:row] objectForKey:@"id"]];
    NSData *removeData = [removeString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    NSString *dataLength = [NSString stringWithFormat:@"%lu",(unsigned long)[removeString length]];
    
    NSURL *removeURL = [NSURL URLWithString:@"http://54.179.143.160/removeProduct.php"];
    NSMutableURLRequest *removeRequest = [NSMutableURLRequest requestWithURL:removeURL];
    
    [removeRequest setHTTPMethod:@"POST"];
    [removeRequest setValue:dataLength forHTTPHeaderField:@"Content-Length"];
    [removeRequest setHTTPBody:removeData];
    
    NSURLConnection *removeConnection = [NSURLConnection connectionWithRequest:removeRequest delegate:self];
    [removeConnection start];
    
    hud.labelText = @"Removing Product";
    [hud show:YES];
}

- (IBAction)proceedToCheckOut:(id)sender {
    if ([cartItems count]==0) {
        [[[UIAlertView alloc] initWithTitle:@"Empty Cart" message:@"Cannot checkout with an empty cart" delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"") otherButtonTitles:nil] show];
        return;
    }
    NSMutableDictionary *checkoutData = [[NSMutableDictionary alloc]
                                         initWithObjects:@[@"0", @"0", @"0", @"0", @"0", @"0", [[NSNumber alloc] initWithBool:NO], [[NSNumber alloc] initWithBool:NO], @"0", @"0", @"0", @"0", @"0", @"0", @"0", [[NSNumber alloc] initWithBool:NO], [[NSNumber alloc] initWithBool:NO], grandTotal]
                                         forKeys:@[@"fname", @"lname", @"email", @"address", @"city", @"phone", @"sameShipping", @"signup", @"password1", @"password2", @"Sfname", @"Slname", @"Saddress", @"city", @"Sphone", @"saveAddressBill", @"saveAddressShip", @"cartTotal"]];
  
    if (appDelegate.isLoggedIn) {
        StyloAddressListViewController *billingAddressPage = (StyloAddressListViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"shippingView"];
        billingAddressPage.isShipping = NO;
        billingAddressPage.checkoutData = checkoutData;
        [self.navigationController pushViewController:billingAddressPage animated:YES];
    }else{
        StyloLoginPageViewController *destinationViewController = (StyloLoginPageViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"loginPage"];
        destinationViewController.isCheckOut = YES;
        destinationViewController.checkoutData = checkoutData;
        [self.navigationController pushViewController:destinationViewController animated:YES];
    }
}

@end
