//
//  StyloSlidingMenuViewController.h
//  Stylo
//
//  Created by Pantera Engineering on 19/04/2014.
//  Copyright (c) 2014 Pantera Engineering. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "ECSlidingViewController.h"
@interface StyloSlidingMenuViewController : UIViewController <UITableViewDataSource, UITableViewDelegate,MFMailComposeViewControllerDelegate>
@property (strong, nonatomic) IBOutlet UITableView *menuTableView;
@property (strong, nonatomic) UINavigationController *navController;

@end
