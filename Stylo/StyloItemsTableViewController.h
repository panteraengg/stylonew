//
//  StyloItemsTableTableViewController.h
//  Stylo
//
//  Created by Pantera Engineering on 19/04/2014.
//  Copyright (c) 2014 Pantera Engineering. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StyloItemsViewController.h"

@interface StyloItemsTableViewController :UIViewController <UICollectionViewDelegate , UICollectionViewDataSource,UIPageViewControllerDataSource>// UITableViewController <UITableViewDelegate>

@property (assign,nonatomic) NSUInteger pageIndex;
@property (nonatomic,strong) NSDictionary* styloType;
@property (nonatomic,strong) StyloItemsViewController *parent;
@property (nonatomic,assign) BOOL pageAnimationFinished;
@end
