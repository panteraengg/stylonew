//
//  StyloItemsViewController.m
//  Stylo
//
//  Created by Pantera Engineering on 19/04/2014.
//  Copyright (c) 2014 Pantera Engineering. All rights reserved.
//

#import "StyloItemsViewController.h"
#import "StyloItemsTableViewController.h"
#import "StyloNavigationViewController.h"
#import "MBProgressHUD.h"
#import "HMSegmentedControl.h"
#import "SDImageCache.h"
typedef void (^CompletionBlock)(NSData*);
typedef void (^errorBlock)(NSError*);
@interface StyloItemsViewController ()
@property (nonatomic,strong) HMSegmentedControl *segmentedControl;
@end

@implementation StyloItemsViewController{
    MBProgressHUD *hud;
    UICollectionView *styloCollectionView;
    NSUInteger currentIndex;
    UIAlertView *alert;
    CompletionBlock block;
    errorBlock theErrorBlock;
    
    NSMutableData *responseData;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationItem setTitle:@"Shoes"];
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Loading";
    
    NSURL *myURL = [NSURL URLWithString:_targetURL];
    
    NSURLRequest *myRequest = [NSURLRequest requestWithURL:myURL];
    
    [NSURLConnection connectionWithRequest:myRequest delegate:self];

  /*  UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"list"] style:UIBarButtonItemStyleBordered target:self.navigationController action:@selector(revealMenu)];
    [self.navigationItem setRightBarButtonItem:barButton];
     alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Error connecting to server" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
   */
}

-(void)moveSegmentedControlToindex:(NSUInteger)index{
    [self.segmentedControl setSelectedSegmentIndex:index animated:YES];
    currentIndex = index;
}
-(void)setPageIndexOnPageViewController:(NSUInteger)index{
    UIPageViewControllerNavigationDirection direction;
    if(currentIndex>index)
    {
        direction = UIPageViewControllerNavigationDirectionReverse;
    } else if(currentIndex<index){
        direction = UIPageViewControllerNavigationDirectionForward;
    } else {
        return;
    }
    StyloItemsTableViewController *ViewControllerToSet = [self viewControllerAtIndex:index];
    NSArray *viewControllers = @[ViewControllerToSet];
    [self.pageViewController setViewControllers:viewControllers direction:direction animated:YES completion:nil];
    
}
-(void)setUpSegmentedControl{
    self.segmentedControl = [[HMSegmentedControl alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    NSMutableArray *sectionTitles;
    currentIndex=0;
    sectionTitles = [[NSMutableArray alloc] init];
    for(int i=0; i<[_styloData count] ; i++)
    {
        [sectionTitles addObject:[[_styloData objectAtIndex:i] objectForKey:@"name"]];
       
    }
    self.segmentedControl.sectionTitles = sectionTitles;
    self.segmentedControl.selectedSegmentIndex = 0;
    self.segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    self.segmentedControl.scrollEnabled = YES;
    self.segmentedControl.selectionIndicatorColor = [UIColor colorWithRed:0.36 green:0.176 blue:0.569 alpha:1];
    self.segmentedControl.textColor = [UIColor lightGrayColor];
    self.segmentedControl.selectedTextColor = [UIColor colorWithRed:0.36 green:0.176 blue:0.569 alpha:1];
    
    self.segmentedControl.backgroundColor = [UIColor colorWithRed:0.945 green:0.912 blue:0.866 alpha:1];
    self.segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    self.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    [_viewForSegments addSubview:_segmentedControl];
    __weak typeof(self) weakSelf = self;
    [self.segmentedControl setIndexChangeBlock:^(NSInteger index) {
        [weakSelf setPageIndexOnPageViewController:index];
    }];
}

-(void)setUpPageViewController{
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"pageViewController"];
    self.pageViewController.dataSource = self;
    self.pageViewController.delegate = self;
    
    StyloItemsTableViewController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers;
    NSLog(@"DFGD : %@" ,startingViewController);
    viewControllers = @[startingViewController];
    
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-94);
    
    [self addChildViewController:_pageViewController];
    [self.viewForPageView addSubview:_pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
    [self setUpSegmentedControl];
}

#pragma mark - URL Connection Data Delegate
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    responseData = [[NSMutableData alloc] init];
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [responseData appendData:data];
    
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
  //  block(responseData);
    //NSLog(@"RESPONSE STRING : %@" , [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
    NSArray *tempArray = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:nil];
    
    _styloData=[[[tempArray reverseObjectEnumerator] allObjects] mutableCopy];
    
    if(_styloData==NULL){
        
        [hud hide:YES];
        [ self viewDidLoad];
        return;
    }
    else if(_styloData.count==0){
        
        [hud hide:YES];
        UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"" message:@"No items are available in this category" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert1 show];
        return;
    }

    [self setUpPageViewController];
    [styloCollectionView reloadData];
    [hud hide:YES];
}
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    [hud hide:YES];
    [alert show];
}

#pragma mark - Connection Methods
/*
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    
    NSArray *tempArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    _styloData=[[[tempArray reverseObjectEnumerator] allObjects] mutableCopy];
    
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {

    [hud hide:YES];
    [alert show];
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    // do something with the data
    if(_styloData==NULL){
        
        [hud hide:YES];
        [ self viewDidLoad];
        return;
    }
    [self setUpPageViewController];
    [styloCollectionView reloadData];
    [hud hide:YES];
    
    
}

*/
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    SDImageCache *imageCache = [SDImageCache sharedImageCache];
    [imageCache clearMemory];
    [imageCache clearDisk];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Alert View Delegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - UICOLLECTION VIEW DATA SOURCE
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [_styloData count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"collectionCell" forIndexPath:indexPath];
    UILabel *label = (UILabel *)[cell viewWithTag:1];
    label.text = [[_styloData objectAtIndex:indexPath.row] objectForKey:@"name"];
    return cell;
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
}

#pragma mark - UI PAGE VIEW CONTROLLER DATA SOURCE
- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSUInteger index = ((StyloItemsTableViewController*) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
    
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    NSUInteger index = ((StyloItemsTableViewController*) viewController).pageIndex;
    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == [self.styloData count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
    
}

- (StyloItemsTableViewController *)viewControllerAtIndex:(NSUInteger)index
{
   // NSLog(@"data : %@ , index  :%lu" , _styloData , (unsigned long)index);
    if (([_styloData count] == 0) || (index >= [_styloData count])) {
        return nil;
    }
    
    // Create a new view controller and pass suitable data.
    StyloItemsTableViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"pageContentController"];
    pageContentViewController.pageIndex = index;
    pageContentViewController.styloType = [_styloData objectAtIndex:index];
    pageContentViewController.parent = self;
    
    return pageContentViewController;
}

@end
