//
//  StyloBillingInformationViewController.h
//  Stylo
//
//  Created by Pantera Engineering on 30/04/2014.
//  Copyright (c) 2014 Pantera Engineering. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface StyloAddressFormViewController : UIViewController <UITextFieldDelegate>


- (IBAction)releaseFocus:(id)sender;

- (IBAction)continueToShipping:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *fname;
@property (strong, nonatomic) IBOutlet UITextField *lname;
@property (strong, nonatomic) IBOutlet UITextField *phoneNumber;
@property (strong, nonatomic) IBOutlet UITextField *email;
@property (strong, nonatomic) IBOutlet UITextField *address;
@property (strong, nonatomic) IBOutlet UITextField *city;

@property (strong, nonatomic) NSMutableArray *data;
@property (assign, nonatomic) BOOL isShipping;
@property (strong, nonatomic) NSMutableDictionary *checkoutData;

@end
