//
//  StyloSlidingMenuViewController.m
//  Stylo
//
//  Created by Pantera Engineering on 19/04/2014.
//  Copyright (c) 2014 Pantera Engineering. All rights reserved.
//

#import "StyloSlidingMenuViewController.h"
#import "StyloLoginPageViewController.h"
#import "StyloAppDelegate.h"
#import "StyloDescriptionViewController.h"

@interface StyloSlidingMenuViewController ()

@end

@implementation StyloSlidingMenuViewController{
    NSMutableArray *menuLoggedInItemTitles;
    NSMutableArray *menuLoggedInItemIcons;
    NSMutableArray *menuLoggedOutItemTitles;
    NSMutableArray *menuLoggedOutItemIcons;
    StyloAppDelegate *appDelegate;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    
   /*[self.slidingViewController setAnchorLeftRevealAmount:280.0f];
    self.slidingViewController.underRightWidthLayout = ECFullWidth;
    */
    [self.slidingViewController setAnchorRightRevealAmount:240.0f];
    self.slidingViewController.underLeftWidthLayout = ECFullWidth;
    appDelegate = [[UIApplication sharedApplication] delegate];
    
    menuLoggedInItemIcons = [[NSMutableArray alloc] init];
    menuLoggedInItemTitles = [[NSMutableArray alloc] init];
    menuLoggedOutItemIcons = [[NSMutableArray alloc] init];
    menuLoggedOutItemTitles = [[NSMutableArray alloc] init];
    
    /*[menuLoggedOutItemTitles addObjectsFromArray:@[ @"Home", @"Shopping Cart", @"Login", @"Shipping Policy", @"Privacy Policy", @"About Stylo", @"Tell your Friends", @"Report a Bug", @"Rate this App"]];
    [menuLoggedOutItemIcons addObjectsFromArray:@[ @"home", @"cart", @"login", @"shipping", @"police", @"info", @"friend", @"bug", @"rate"]]; */
    
    [menuLoggedOutItemTitles addObjectsFromArray:@[ @"Home", @"Store Locator",@"Login", @"Shipping Policy",@"Privacy Policy",  @"About",@"Tell your Friends", @"Report a Bug",@"Rate this App",@"Loyalty Cards",@"Repair Wing", @"Settings"]];
    [menuLoggedOutItemIcons addObjectsFromArray:@[ @"home", @"cart", @"login", @"shipping", @"police", @"info", @"friend", @"bug", @"rate", @"rate", @"rate", @"rate"]];
    
    [menuLoggedInItemTitles addObjectsFromArray:@[ @"Home", @"Shopping Cart", @"Logout", @"Shipping Policy", @"Privacy Policy", @"About Stylo", @"Tell a Friend", @"Report a Bug", @"Rate this App"]];
    [menuLoggedInItemIcons addObjectsFromArray:@[ @"home", @"cart", @"logout", @"shipping", @"police", @"info", @"friend", @"bug", @"rate"]];
    
    self.menuTableView.dataSource = self;
    self.menuTableView.delegate = self;
}

-(void)openView:(NSString *)uid{
    
    NSString *identifier = uid;
    
    UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:identifier];
    
    [self.slidingViewController anchorTopViewOffScreenTo:ECLeft /*ECRight*/ animations:nil onComplete:^{
        CGRect frame = self.slidingViewController.topViewController.view.frame;
        self.slidingViewController.topViewController = newTopViewController;
        self.slidingViewController.topViewController.view.frame = frame;
        [self.slidingViewController resetTopView];
    }];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - TABLE VIEW DATA SOURCE

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (appDelegate.isLoggedIn) {
        return [menuLoggedInItemTitles count];
    }else{
        return [menuLoggedOutItemTitles count];
    }
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *aCell = [tableView dequeueReusableCellWithIdentifier:@"homeCell"];
    
    UILabel *label = (UILabel *)[aCell viewWithTag:1];
    UIImageView *icon = (UIImageView *)[aCell viewWithTag:2];

    
    if(appDelegate.isLoggedIn){
        label.text = [menuLoggedInItemTitles objectAtIndex:indexPath.row];
        icon.image = [UIImage imageNamed:[menuLoggedInItemIcons objectAtIndex:indexPath.row]];
    }else{
        label.text = [menuLoggedOutItemTitles objectAtIndex:indexPath.row];
        icon.image = [UIImage imageNamed:[menuLoggedOutItemIcons objectAtIndex:indexPath.row]];
    }
    icon.image = [icon.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [icon setTintColor:[UIColor whiteColor]];
    return aCell;
}

#pragma mark - Table View Delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.slidingViewController anchorTopViewOffScreenTo:ECRight/*ECLeft*/ animations:nil onComplete:^{
        [self.slidingViewController resetTopView];
        if(indexPath.row == 0){
            [_navController popToRootViewControllerAnimated:YES];
        }else if(indexPath.row==1){
            [_navController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"cartView"] animated:YES];
        }else if(indexPath.row == 2 && ![[[_navController viewControllers] lastObject] isKindOfClass:[StyloLoginPageViewController class]]){
            if (!appDelegate.isLoggedIn) {
                [_navController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"loginPage"] animated:YES];
            }else{
                NSURL *url = [NSURL URLWithString:@"http://54.179.143.160/logout.php"];
                NSURLRequest *request = [NSURLRequest requestWithURL:url];
                NSURLConnection *connection = [NSURLConnection connectionWithRequest:request delegate:self];
                [connection start];
            }
        }else if ((indexPath.row >= 3) && (indexPath.row <=5)){
            StyloDescriptionViewController *descController = [self.storyboard instantiateViewControllerWithIdentifier:@"descriptionView"];
            descController.descriptionType = (int)indexPath.row - 3;
            [_navController pushViewController:descController animated:YES];
        }else if(indexPath.row == 6){
            UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:@[@"Hey! check out this amazing application for stylo shoes. I have used it and I love it. You can easily browse through the entire catalogue of Stylo Shoes and buy your favorite one easily with cash on delivery. Do checkout the app from the provided link. It is absolutely free to download\n", [NSURL URLWithString:@"http://panteraengineering.com"], [UIImage imageNamed:@"styloLogo"]] applicationActivities:nil];
            [controller setExcludedActivityTypes:@[UIActivityTypeCopyToPasteboard, UIActivityTypeSaveToCameraRoll, UIActivityTypeAssignToContact, UIActivityTypePostToVimeo, UIActivityTypePrint, UIActivityTypeAssignToContact, UIActivityTypeAirDrop, UIActivityTypeAddToReadingList]];
            [self presentViewController:controller animated:YES completion:nil];
        }else if (indexPath.row == 7){
            if ([MFMailComposeViewController canSendMail])
            {
                MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
                
                mailer.mailComposeDelegate = self;
                
                [mailer setSubject:@"[Bug Report] Found a bug in Stylo iOS App"];
                
                NSArray *toRecipients = [NSArray arrayWithObjects:@"ttanveer@panteraengineering.com", @"info@styloshoes.com", nil];
                [mailer setToRecipients:toRecipients];
                
                NSString *emailBody = @"Hey! I have found a bug in your application for Stylo on iOS. The details of the bug are as follows:\n";
                [mailer setMessageBody:emailBody isHTML:NO];
                
                [self presentViewController:mailer animated:YES completion:nil];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure" message:@"Your device doesn't support the Email composer sheet" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
            }

            
        }else if (indexPath.row == 8){
            NSString *str = @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=337064413"; //replace the id param's value with your App's id
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
        }
    }];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

}

#pragma mark - Connection Methods
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)d {
    //[self.data addObject:d];
    NSDictionary *response = [NSJSONSerialization JSONObjectWithData:d options:NSJSONReadingMutableContainers error:nil];
    
    if ([[response objectForKey:@"code"] isEqual:@"100"]) {
        appDelegate.isLoggedIn = NO;
        [self.menuTableView reloadData];
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Logout Error", @"")
                                message:[error localizedDescription]
                               delegate:nil
                      cancelButtonTitle:NSLocalizedString(@"OK", @"")
                      otherButtonTitles:nil] show];
}

#pragma mark - mail composer Delegate
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [controller dismissViewControllerAnimated:YES completion:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
