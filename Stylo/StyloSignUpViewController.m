//
//  StyloSignUpViewController.m
//  Stylo
//
//  Created by Pantera Engineering on 05/05/2014.
//  Copyright (c) 2014 Pantera Engineering. All rights reserved.
//

#import "StyloSignUpViewController.h"
#import "MBProgressHUD.h"

@interface StyloSignUpViewController ()

@end

@implementation StyloSignUpViewController{
    UIColor *styloPrimaryColor;
    UIColor *styloSecondaryColor;
    NSMutableURLRequest *signupRequest;
    MBProgressHUD *hud;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 20)];
    styloPrimaryColor = [UIColor colorWithRed:92/255.0 green:45/255.0 blue:145/255.0 alpha:1];
    styloSecondaryColor = [UIColor colorWithRed:243/255.0 green:145/255.0 blue:188/255.0 alpha:1];
    
    CGRect frameRect1 = _fName.frame;
    CGRect frameRect2 = _lName.frame;
    CGRect frameRect3 = _emailAddress.frame;
    CGRect frameRect4 = _password1.frame;
    CGRect frameRect5 = _password2.frame;
    frameRect1.size.height = 40;
    frameRect2.size.height = 40;
    frameRect3.size.height = 40;
    frameRect4.size.height = 40;
    frameRect5.size.height = 40;
    _fName.frame = frameRect1;
    _lName.frame=frameRect2;
    _emailAddress.frame=frameRect3;
    _password1.frame=frameRect4;
    _password2.frame=frameRect5;
   /* for(id x in [self.view subviews]){
        if([x isKindOfClass:[UITextField class]])
        {
            UITextField *textField = x;
            textField.layer.borderColor = [styloSecondaryColor CGColor];
            textField.layer.borderWidth = 1.f;
            textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: styloSecondaryColor}];
            textField.leftView = paddingView;
            textField.layer.sublayerTransform = CATransform3DMakeTranslation(5.5, 0, 0);
        }
    }
    */
    
    self.navigationItem.title = @"SignUp";

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)shakeItBaby:(UIView *)viewToShake{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
    [animation setDuration:0.05];
    [animation setRepeatCount:4];
    [animation setAutoreverses:YES];
    [animation setFromValue:[NSValue valueWithCGPoint:
                             CGPointMake([viewToShake center].x - 5.0f, [viewToShake center].y)]];
    [animation setToValue:[NSValue valueWithCGPoint:
                           CGPointMake([viewToShake center].x + 5.0f, [viewToShake center].y)]];
    
    [viewToShake.layer addAnimation:animation forKey:@"key"];
}
-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)signup{
    NSURL *targetUrl;
    targetUrl = [NSURL URLWithString:@"http://54.179.143.160/signup.php"];
    signupRequest = [NSMutableURLRequest requestWithURL:targetUrl ];

    NSString *post = [NSString stringWithFormat:@"fname=%@&lname=%@&email=%@&password1=%@&password2=%@",_fName.text,_lName.text,_emailAddress.text,_password1.text,_password2.text];
    
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    [signupRequest setHTTPMethod:@"POST"];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[post length]];
    [signupRequest setValue:postLength forHTTPHeaderField:@"Content-Length"];
    /*[signupRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Current-Type"];*/
    
    [signupRequest setHTTPBody:postData];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:signupRequest delegate:self];
    [connection start];
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Signing Up";
}
-(void)signedUpSuccessfullyWithMessage:(NSString *)message{
    hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
	
	// Set custom view mode
	hud.mode = MBProgressHUDModeCustomView;
	
	hud.labelText = @"Completed";
	[hud hide:YES afterDelay:1];
    [self performSelector:@selector(goToRoot) withObject:nil afterDelay:1];
}
-(void)goToRoot{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(void)signUpFailedWithErrorMessage:(NSString *)message{
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error Signing Up", @"")
                                message:message
                               delegate:nil
                      cancelButtonTitle:NSLocalizedString(@"OK", @"")
                      otherButtonTitles:nil] show];
}
#pragma mark - Connection Methods
 - (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
 //[self.data removeAllObjects];
 }
 
 - (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)d {
 //[self.data addObject:d];
     NSDictionary *response = [NSJSONSerialization JSONObjectWithData:d options:NSJSONReadingMutableContainers error:nil];
     NSLog(@"response: %@", response);
     [hud hide:YES];
     if ([[response objectForKey:@"code"]  isEqual:@"100"]) {
         [self signedUpSuccessfullyWithMessage:[response objectForKey:@"message"]];
     }else{
         [self signUpFailedWithErrorMessage:[response objectForKey:@"message"]];
     }
 }
 
 - (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"")
                                    message:[error localizedDescription]
                                   delegate:nil
                          cancelButtonTitle:NSLocalizedString(@"OK", @"")
                          otherButtonTitles:nil] show];
 }
 
 - (void)connectionDidFinishLoading:(NSURLConnection *)connection {
 
 // Do anything you want with it
 
 }
 
 /*// Handle basic authentication challenge if needed
 - (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
 NSString *username = @"username";
 NSString *password = @"password";
 
 NSURLCredential *credential = [NSURLCredential credentialWithUser:username
 password:password
 persistence:NSURLCredentialPersistenceForSession];
 [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
 }*/
 
#pragma mark - Interface Builder actions
- (IBAction)attemptSignUp:(id)sender {
    if(_fName.text.length==0){
        [self shakeItBaby:_fName];
    }else if(_lName.text.length==0){
        [self shakeItBaby:_lName];
    }else if (![self NSStringIsValidEmail:_emailAddress.text]){
        [self shakeItBaby:_emailAddress];
    }else if (_password1.text.length<7){
        [self shakeItBaby:_password1];
    }else if (![_password2.text isEqualToString:_password1.text]){
        [self shakeItBaby:_password2];
    }else{
        [self signup];
    }
}

- (IBAction)releaseFocus:(id)sender {
    [self.view endEditing:YES];
}

#pragma mark - Text field delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField.tag >= 100 && textField.tag<105){
        [[self.view viewWithTag:textField.tag+1] becomeFirstResponder];
    }else if (textField.tag == 105){
        [self.view endEditing:YES];
    }
    return YES;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    if(textField.text.length >0){
        textField.layer.borderColor = [styloSecondaryColor CGColor];
    }else{
        textField.layer.borderColor = [styloSecondaryColor CGColor];
    }
    textField.layer.borderWidth = 1.0f;
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    textField.clipsToBounds = NO;
    
    textField.layer.borderColor = [styloPrimaryColor CGColor];
    textField.layer.borderWidth = 2.0f;
    
    return YES;
}

@end
