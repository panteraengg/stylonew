//
//  StyloDescriptionViewController.m
//  Stylo
//
//  Created by Pantera Engineering on 07/05/2014.
//  Copyright (c) 2014 Pantera Engineering. All rights reserved.
//

#import "StyloDescriptionViewController.h"
#include "StyloNavigationViewController.h"

@interface StyloDescriptionViewController ()

@end

@implementation StyloDescriptionViewController{
    NSArray *titles;
    NSArray *fileNames;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    titles = @[@"Shipping Policy", @"Privacy Poliy", @"About Stylo"];
    fileNames = @[@"shipping",@"privacy",@"about"];
    
    [self.navigationItem setTitle:[titles objectAtIndex:_descriptionType]];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:[fileNames objectAtIndex:_descriptionType] ofType:@"txt"];
    NSString *content = [NSString stringWithContentsOfFile:path
                                                  encoding:NSUTF8StringEncoding
                                                     error:NULL];
    _descriptionText.text = content;
 /*   UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"list"] style:UIBarButtonItemStyleBordered target:self.navigationController action:@selector(revealMenu)];
    [self.navigationItem setRightBarButtonItem:barButton];
*/
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
