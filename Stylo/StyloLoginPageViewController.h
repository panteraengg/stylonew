//
//  StyloLoginPageViewController.h
//  Stylo
//
//  Created by Pantera Engineering on 30/04/2014.
//  Copyright (c) 2014 Pantera Engineering. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StyloLoginPageViewController : UIViewController<UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITextField *emailField;
@property (strong, nonatomic) IBOutlet UITextField *passwordField;
@property (strong, nonatomic) IBOutlet UIButton *createAccountButton;
@property (strong, nonatomic) NSMutableDictionary *checkoutData;

@property (assign, nonatomic) BOOL isCheckOut;
- (IBAction)login:(id)sender;
- (IBAction)stopEditingIt:(id)sender;
- (IBAction)createAccount:(id)sender;

@end
