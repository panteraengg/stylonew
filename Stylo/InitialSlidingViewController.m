//
//  InitialSlidingViewController.m
//  ECSlidingViewController
//
//  Created by Michael Enriquez on 1/25/12.
//  Copyright (c) 2012 EdgeCase. All rights reserved.
//

#import "InitialSlidingViewController.h"
#import "StyloAppDelegate.h"
#import "StyloNavigationViewController.h"

@implementation InitialSlidingViewController{
    StyloAppDelegate *appDelegate;
    StyloNavigationViewController *navController;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    UIStoryboard *storyboard;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    } else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        storyboard = [UIStoryboard storyboardWithName:@"iPad" bundle:nil];
    }
    navController = (StyloNavigationViewController *)[storyboard instantiateViewControllerWithIdentifier:@"startView"];
    self.topViewController = navController;
    NSURL *myURL = [NSURL URLWithString:@"http://54.179.143.160/checkLogin.php"];
    
    NSURLRequest *myRequest = [NSURLRequest requestWithURL:myURL];
    
    [[NSURLConnection connectionWithRequest:myRequest delegate:self] start];
    appDelegate = [[UIApplication sharedApplication] delegate];
}
#pragma mark - Connection Methods

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    
    NSDictionary *tempArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    if ([[tempArray objectForKey:@"code"] isEqual:@"100"]) {
        appDelegate.isLoggedIn = YES;
    }else{
        appDelegate.isLoggedIn = NO;
    }
    
    [navController reloadMenu];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    appDelegate.isLoggedIn = NO;
    [navController reloadMenu];
    
}


@end
